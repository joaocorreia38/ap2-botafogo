window.onload = () => {
    let player = document.getElementById("player-detail")
    let i = document.cookie.split('*')
    const img = i[0];
    const nome = i[1];
    const nascimento = i[2];
    const altura_peso = i[3];

    player.innerHTML = 
    `
    <div class="track-info">
    <img src='${img}'>
    <h2>${nome}</h2>
    <p>${nascimento}</p>
    <p>${altura_peso}</p>
    </div>
    `;
}


